fn say_hello (name: &str) {
    println!("Hello, {}!", name);
}

fn main() {
    let _x = 1;
    let _y = 2.0;
    let my_tuple = (_x, "crap", "abracadabra");
    let (_, _, _word) = my_tuple;
    say_hello("Renato");
    println!("The word is {}", _word);
}
